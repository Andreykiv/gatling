import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class Record1 extends Simulation {

	val httpProtocol = http
		.baseURL("https://qa.reports.spd-ukraine.com")
		.inferHtmlResources(BlackList(), WhiteList())
		.acceptHeader("application/json, text/javascript, */*; q=0.01")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("uk,ru;q=0.8,en-US;q=0.5,en;q=0.3")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0")

	val headers_0 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_1 = Map(
		"Accept" -> "*/*",
		"Authorization" -> "Basic c3lzdGVtVUk6S01oNVJWeFBBREZXR05tTg==",
		"Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_2 = Map(
		"Accept" -> "*/*",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_3 = Map(
		"Accept" -> "text/html, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_5 = Map(
		"Accept" -> "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_7 = Map(
		"Authorization" -> "Bearer 31c33bf0-4092-4a82-8400-8cf1f9e7f20c",
		"Content-Type" -> "application/json")

    val uri1 = "https://qa.reports.spd-ukraine.com:443"

	val scn = scenario("Record1")
		.exec(http("Login page")
			.get("/login")
			.headers(headers_0))
		.pause(17)
		.exec(http("request_1")
			.post("/api/oauth/token")
			.headers(headers_1)
			.formParam("grant_type", "password")
			.formParam("username", "andreykiv@gmail.com")
			.formParam("scope[]", "read")
			.formParam("scope[]", "write")
			.formParam("password", "1598753")
			.basicAuth("systemUI","KMh5RVxPADFWGNmN")
			.resources(http("request_2")
			.post("/login")
			.headers(headers_2),
            http("request_3")
			.get("/?isAjax=true")
			.headers(headers_3),
            http("request_4")
			.get("/trackTime?isAjax")
			.headers(headers_3),
            http("request_5")
			.get("/resources/js/main.js?_v=1519153695917&_=1520186643894")
			.headers(headers_5),
            http("request_6")
			.get("/resources/js/main.js?_v=1519153695917&_=1520186643895")
			.headers(headers_5),
            http("request_7")
			.get("/api/api/v1/projects/my?_=1520186643897")
			.headers(headers_7),
            http("request_8")
			.get("/api/api/v1/userSettings?_=1520186643896")
			.headers(headers_7),
            http("request_9")
			.get("/api/api/v1/jira/getProjectsWithIntegrationEnabled?_=1520186643899")
			.headers(headers_7),
            http("request_10")
			.get("/api/api/v1/tasks/csvSettings?_=1520186643898")
			.headers(headers_7),
            http("request_11")
			.get("/api/api/v1/tasks?dateFrom=2018-02-26&dateTo=2018-03-04&_=1520186643900")
			.headers(headers_7),
            http("request_12")
			.get("/api/api/v1/dayStatuses?dateFrom=2018-02-26&dateTo=2018-03-04&_=1520186643901")
			.headers(headers_7),
            http("request_13")
			.get("/api/api/v1/userLockWeek/unlocked?date=2018-02-26&_=1520186643902")
			.headers(headers_7),
            http("request_14")
			.get("/api/api/v1/tasks?dateFrom=2018-03-04&_=1520186643903")
			.headers(headers_7),
            http("request_15")
			.get("/api/api/v1/tasks?dateFrom=2018-03-04&_=1520186643904")
			.headers(headers_7),
            http("request_16")
			.get("/api/api/v1/dayStatuses?dateFrom=2018-03-04&dateTo=2018-03-04&_=1520186643905")
			.headers(headers_7),
            http("request_17")
			.get("/api/api/v1/userLockWeek/unlocked?date=2018-03-04&_=1520186643906")
			.headers(headers_7)))
		.pause(6)
		.exec(http("request_18")
			.get("/api/api/v1/tasks/autosuggestion?name=7&_=1520186643907")
			.headers(headers_7))
		.pause(2)
		.exec(http("request_19")
			.post("/api/api/v1/tasks/all")
			.headers(headers_7)
			.body(RawFileBody("Record1_0019_request.txt")))
		.pause(2)
		.exec(http("request_20")
			.get("/trackTime")
			.headers(headers_0)
			.resources(http("request_21")
			.post("/login")
			.headers(headers_2),
            http("request_22")
			.get("/api/api/v1/projects/my?_=1520186675345")
			.headers(headers_7),
            http("request_23")
			.get("/api/api/v1/tasks/csvSettings?_=1520186675346")
			.headers(headers_7),
            http("request_24")
			.get("/api/api/v1/userSettings?_=1520186675344")
			.headers(headers_7),
            http("request_25")
			.get("/api/api/v1/jira/getProjectsWithIntegrationEnabled?_=1520186675347")
			.headers(headers_7),
            http("request_26")
			.get("/api/api/v1/tasks?dateFrom=2018-02-26&dateTo=2018-03-04&_=1520186675348")
			.headers(headers_7),
            http("request_27")
			.get("/api/api/v1/dayStatuses?dateFrom=2018-02-26&dateTo=2018-03-04&_=1520186675349")
			.headers(headers_7),
            http("request_28")
			.get("/api/api/v1/userLockWeek/unlocked?date=2018-02-26&_=1520186675350")
			.headers(headers_7),
            http("request_29")
			.get("/api/api/v1/tasks?dateFrom=2018-03-04&_=1520186675351")
			.headers(headers_7),
            http("request_30")
			.get("/api/api/v1/tasks?dateFrom=2018-03-04&_=1520186675352")
			.headers(headers_7),
            http("request_31")
			.get("/api/api/v1/dayStatuses?dateFrom=2018-03-04&dateTo=2018-03-04&_=1520186675353")
			.headers(headers_7),
            http("request_32")
			.get("/api/api/v1/userLockWeek/unlocked?date=2018-03-04&_=1520186675354")
			.headers(headers_7)))
		.pause(3)
		.exec(http("Open calendar")
			.get("/calendar")
			.headers(headers_0)
			.resources(http("request_34")
			.post("/login")
			.headers(headers_2),
            http("request_35")
			.get("/api/api/v1/userSettings?_=1520186679803")
			.headers(headers_7),
            http("request_36")
			.get("/api/api/v1/projects?_=1520186679802")
			.headers(headers_7),
            http("request_37")
			.get("/api/api/v1/dayStatuses/userDayStatus?userId=11&confirmFilter=PENDING&confirmFilter=DENIED&_=1520186679804")
			.headers(headers_7),
            http("request_38")
			.get("/api/api/v1/calendar?dateFrom=2018-02-26&dateTo=2018-04-02&projectId=1&onlyInTeam=true&statusTypes=VACATION&statusTypes=ILLNESS&statusTypes=BIRTHDAY&statusTypes=HOLIDAY&statusTypes=WORKING_DAY&statusTypes=EVENT&_=1520186679805")
			.headers(headers_7)))
		.pause(1)
		.exec(http("Open statistics")
			.get("/statistics")
			.headers(headers_0)
			.resources(http("request_40")
			.post("/login")
			.headers(headers_2),
            http("request_41")
			.get("/api/api/v1/statistics?_=1520186682296")
			.headers(headers_7),
            http("request_42")
			.get("/users")
			.headers(headers_0),
            http("request_43")
			.post("/login")
			.headers(headers_2),
            http("request_44")
			.get("/api/api/v1/addresses/officeLocations?_=1520186683890")
			.headers(headers_7),
            http("request_45")
			.get("/api/api/v1/projects?_=1520186683889")
			.headers(headers_7),
            http("request_46")
			.get("/api/api/v1/users/export/xls/test?activeUsers=true&project=1&_=1520186683891")
			.headers(headers_7),
            http("request_47")
			.get("/api/api/v1/users?page=1&limit=20&activeUsers=true&project=1&_=1520186683892")
			.headers(headers_7),
            http("Open trackTime page")
			.get("/trackTime")
			.headers(headers_0),
            http("request_49")
			.post("/login")
			.headers(headers_2),
            http("request_50")
			.get("/api/api/v1/userSettings?_=1520186686026")
			.headers(headers_7),
            http("request_51")
			.get("/api/api/v1/jira/getProjectsWithIntegrationEnabled?_=1520186686029")
			.headers(headers_7),
            http("request_52")
			.get("/api/api/v1/tasks/csvSettings?_=1520186686028")
			.headers(headers_7),
            http("request_53")
			.get("/api/api/v1/projects/my?_=1520186686027")
			.headers(headers_7),
            http("request_54")
			.get("/api/api/v1/tasks?dateFrom=2018-02-26&dateTo=2018-03-04&_=1520186686030")
			.headers(headers_7),
            http("request_55")
			.get("/api/api/v1/dayStatuses?dateFrom=2018-02-26&dateTo=2018-03-04&_=1520186686031")
			.headers(headers_7),
            http("request_56")
			.get("/api/api/v1/userLockWeek/unlocked?date=2018-02-26&_=1520186686032")
			.headers(headers_7),
            http("request_57")
			.get("/api/api/v1/tasks?dateFrom=2018-03-04&_=1520186686033")
			.headers(headers_7),
            http("request_58")
			.get("/api/api/v1/dayStatuses?dateFrom=2018-03-04&dateTo=2018-03-04&_=1520186686035")
			.headers(headers_7),
            http("request_59")
			.get("/api/api/v1/tasks?dateFrom=2018-03-04&_=1520186686034")
			.headers(headers_7),
            http("request_60")
			.get("/api/api/v1/userLockWeek/unlocked?date=2018-03-04&_=1520186686036")
			.headers(headers_7)))
		.pause(4)
		.exec(http("request_61")
			.delete("/api/api/v1/tasks/1216")
			.headers(headers_7))
		.pause(3)
		.exec(http("request_62")
			.post("/api/api/v1/tasks/all")
			.headers(headers_7)
			.body(RawFileBody("Record1_0062_request.txt")))
		.pause(2)
		.exec(http("equest_63")
			.get("/login")
			.headers(headers_0))

	setUp(scn.inject(atOnceUsers(25))).protocols(httpProtocol)
}